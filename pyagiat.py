#!/usr/bin/env python3

import sys, argparse
import zlib, bz2, lzma


COMPRESSORS = {'zlib':  zlib.compress,
               'zlib9': (lambda data: zlib.compress(data, level=9)),
               'bz2':   bz2.compress,
               'lzma':  lzma.compress}


# normalized compression distance
def dist(A: bytes, B: bytes, compress=bz2.compress) -> float:
    s1  = len(compress(A))
    s2  = len(compress(B))
    s12 = len(compress(A+B))
    # max(s1,s2) <= s12 <= s1+s2
    # max-min <= s12-min <= max
    # 1-min/max <= (s12-min)/max <= 1
    return (s12-s1)/s2 if s1 < s2 else (s12-s2)/s1

def compute_scores(Afiles, Bfiles, compress=bz2.compress):
    symmetric = (Afiles == Bfiles)
    file_content = lambda fname: open(fname, 'rb').read()
    scores = [[0.]*len(Bfiles) for _ in range(len(Afiles))]
    for i in range(len(Afiles)):
        A = file_content(Afiles[i])
        for j in range(len(Bfiles)):
            if Afiles[i] == Bfiles[j]:
                continue
            if symmetric and j<i:
                scores[i][j] = scores[j][i]
            else:
                B = file_content(Bfiles[j])
                scores[i][j] = dist(A, B, compress)
    return scores

def csv_output(Afiles, Bfiles, scores, sep=', '):
    O = [[''] + Bfiles]
    for i in range(len(scores)):
        O.append([Afiles[i]] + [f'{s:.3f}' for s in scores[i]])
    return '\n'.join(sep.join(L) for L in O)


if __name__=='__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('file_names', metavar='file', type=str, nargs='+',
                        help='files to analyze')
    parser.add_argument('-B', metavar='b_file', type=str, nargs='+',
                        help='second set of files to compare to the first (default: same as first set)')
    parser.add_argument('-z', choices=COMPRESSORS.keys(), default='zlib',
                        help='compression algorithm to use (default: zlib)')
    args = parser.parse_args()
    algo = args.z
    Afiles = args.file_names
    Bfiles = args.B if args.B else Afiles
    compress = COMPRESSORS[algo]
    scores = compute_scores(Afiles, Bfiles, compress)
    print(csv_output(Afiles, Bfiles, scores))
