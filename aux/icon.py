#!/usr/bin/env python3

SIZE = 12

from PIL import Image
from math import exp
import random
random.seed()

COL10 = (0+50, 130+40, 110+30)
COL05 = (255, 0, 0)
def intercol(a, col0=COL05, col1=COL10):
    return tuple(round(a*b1+(1.-a)*b0) for b0,b1 in zip(col0, col1))

smooth = lambda x: 1./(1.+exp((0.1-x)/0.1))

if __name__=='__main__':
    Img = Image.new('RGB', (SIZE,SIZE), (255,255,255))
    Pix = Img.load()
    for i in range(SIZE):
        for j in range(i):
            Pix[i,j] = Pix[j,i] = intercol(smooth(random.random()))
    Img.resize((10*SIZE, 10*SIZE), resample=Image.NEAREST).save('_icon.png')
