#!/usr/bin/env python3

##
# Generates a random corpus of texts for testing/benchmarking purposes
#

import sys, os
import random
random.seed()


## CONFIG ##
dict_file = '/usr/share/dict/words'
words = [w.strip() for w in open(dict_file, 'r').readlines()]

file_count  = 15
lines_count = 30
line_size   = 7
seed_count = 5*file_count*line_size

output_dir_name = 'bench'
file_name_format = output_dir_name + '/test{:02d}'
##


def rand_text_file(name, idx):
    F = open(name, 'w')
    for i in range(lines_count):
        random.seed(_seeds[(idx+i)%seed_count])
        line = ' '.join(random.choice(words) for _ in range(line_size))
        F.write(line)
        F.write('\n')
    F.close()

def rand_size():
    return file_size + random.randint(-file_var_size, file_var_size)


if __name__=='__main__':
    try:
        os.mkdir(output_dir_name)
    except FileExistsError:
        pass
    _seeds = [random.randint(0, 1<<30) for _ in range(seed_count)]
    for i in range(file_count):
        rand_text_file(file_name_format.format(i), random.randint(0, seed_count-1))
