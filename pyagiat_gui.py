#!/usr/bin/env python3

import sys, argparse, os.path


## CONFIG. ##
PORT = 8000
PATH = os.path.dirname(__file__)
CSS_FILE = os.path.join(PATH, 'aux', 'style.css')
COL10 = (0, 130, 110)
COL05 = (255, 0, 0)
##


## IMPORTS ##
from http.server import HTTPServer, BaseHTTPRequestHandler
import urllib.parse as urlparse
from threading import Thread
import webbrowser

import matplotlib, io, base64
matplotlib.use('AGG')
import matplotlib.pyplot as plt

from pyagiat import compute_scores, COMPRESSORS
from difflib import HtmlDiff


## HELPERS ##
def intercol(a, col0=COL05, col1=COL10):
    return tuple(round(a*b1+(1.-a)*b0) for b0,b1 in zip(col0, col1))


## SCORES ##
class PyagiatConfig:
    def __init__(self, Afiles, Bfiles):
        self.Afiles = Afiles
        self.Asize = len(self.Afiles)
        self.Bfiles = Bfiles
        self.Bsize = len(self.Bfiles)
        self.symmetric = (self.Afiles == self.Bfiles)
        self.compress = 'zlib'
        self.do_plot = True
        self.scores = None
        self.img = None
        self.update()
    
    def update(self, params=None):
        if params is not None and 'u' in params:
            if 'z' in params:
                algo = params['z'][-1]
                if algo != self.compress and algo in COMPRESSORS:
                    self.compress = algo
                    self.scores = None
            self.do_plot = ('g' in params)
        if self.scores is None:
            self.scores = compute_scores(self.Afiles, self.Bfiles,
                                         COMPRESSORS[self.compress])
            self.img = None
        if self.do_plot and self.img is None:
            self._plot()
    
    def form(self):
        sep = '<b>&nbsp;&nbsp;>&nbsp;&nbsp;</b>'  # separator bullet
        # init form
        form  = ['<form method="get">',
                 '<b>Configuration</b>',
                 sep]
        # select compressor
        form += ['<label for="z">Compresseur&nbsp;:&nbsp;</label>',
                 '<select name="z" id="z">']
        for algo in COMPRESSORS.keys():
            selected = ' selected' if algo == self.compress else ''
            form.append(f'<option value="{algo}"{selected}>{algo}</option>')
        form += ['</select>', sep]
        # checkbox for graph
        checked = ' checked' if self.do_plot else ''
        form += [f'<label for="g">Graphique&nbsp;:&nbsp;</label><input type="checkbox" name="g" id="g"{checked}>', sep]
        # hidden input to detect when we need to update
        form.append('<input type="hidden" name="u" value="p">')
        # finalize form
        form += ['<input type="submit" value="Actualiser">',
                 '</form>']
        return ''.join(form)
    
    def html(self):
        L = ['<tr>', '<td></td>']
        for j in range(self.Bsize):
            L.append(f'<td class="col" title="{self.Bfiles[j]}">B{j:03d}</td>')
        O = ['<table class="tscores">', ''.join(L)]
        for i in range(self.Asize):
            L = ['<tr>', f'<td class="row" title="{self.Afiles[i]}">A{i:03d}</td>']
            for j in range(self.Bsize):
                L.append('<td class="scr">')
                s = self.scores[i][j]
                if s != 0.:
                    r,g,b = intercol(min(s, 1.))
                    L.append(f'<a href="/diff?f1={i}&f2={j}" style="color: rgb({r},{g},{b});">{s:.3f}</a>')
                L.append('</td>')
            L.append('</tr>')
            O.append(''.join(L))
        O.append('</table>')
        return '\n'.join(O)
    
    def _plot(self, width=10.):
        # global histogram
        fig, (ax1, ax2) = plt.subplots(1, 2)
        fig.set_size_inches(width, 3./8.*width)
        hist_data = [self.scores[i][j]
                     for i in range(self.Asize)
                     for j in range(i if self.symmetric else self.Bsize)]
        ax1.set_xlabel('distance')
        ax1.set_ylabel('#paires (A,B)')
        ax1.hist(hist_data, bins=50, range=(0., 1.))
        ax1.set_xlim(0., 1.)
        ax1.set_xticks([x/10. for x in range(11)])
        ax1.set_ylim(0)
        # per file scatter
        x_data = []
        y_data = []
        for i in range(self.Asize):
            for j in range(self.Bsize):
                if i != j:
                    x_data.append(i)
                    y_data.append(self.scores[i][j])
        ax2.set_xlabel('fichiers A')
        ax2.set_ylabel('distances à B')
        ax2.set_xticks(range(0, self.Asize))
        ax2.set_ylim(0., 1.)
        ax2.scatter(x_data, y_data, marker='.')
        plt.tight_layout()
        self.img = io.BytesIO()
        plt.savefig(self.img)
    
    def plot(self):
        img_data = base64.b64encode(self.img.getvalue())
        return f'<img src="data:image/png;base64,{img_data.decode()}">'
    
    def _diff(self, i, j):
        assert 0 <= i < self.Asize
        assert 0 <= j < self.Bsize
        A = open(self.Afiles[i], 'r').readlines()
        B = open(self.Bfiles[j], 'r').readlines()
        title = f'<h1>{_Conf.Afiles[i]} vs. {_Conf.Bfiles[j]}</h1>'
        table = HtmlDiff().make_table(A, B)
        return title + table
    
    def diff(self, params):
        i = int(params['f1'][-1])
        j = int(params['f2'][-1])
        return self._diff(i, j)


## SERVER ##
class PyagiatHTTP(BaseHTTPRequestHandler):
    def do_GET(self):
        url = urlparse.urlparse(self.path)
        params = urlparse.parse_qs(url.query)
        if url.path == '/diff':
            try:
                content = self.generate_diff(params)
            except (KeyError, ValueError, AssertionError):
                content = self.generate_index()
        else:
            _Conf.update(params)
            content = self.generate_index()
        self.send_response(200)
        self.send_header('Content-Type', 'text/html')
        self.send_header('Content-Length', str(len(content)))
        self.end_headers()
        self.wfile.write(content)
    
    def generate_index(self):
        css = open(CSS_FILE, 'rb').read()
        content = [b'<html>', b'<head>', b'<meta charset="utf-8">',
                   b'<title>pyagiat</title>',
                   b'<style>', css, b'</style>',
                   b'</head>', b'<body>',
                   b'<h1>Matrice des distances de compression</h1>']
        form  = _Conf.form()
        table = _Conf.html()
        content += [form.encode(),
                    b'<div class="content">',
                    table.encode()]
        if _Conf.do_plot:
            content.append(_Conf.plot().encode())
        content += [b'</div>', b'</body>', b'</html>']
        return b'\n'.join(content)
    
    def generate_diff(self, params):
        css = open(CSS_FILE, 'rb').read()
        content = [b'<html>', b'<head>', b'<meta charset="utf-8">'
                   b'<title>pyagiat</title>',
                   b'<style>', css, b'</style>',
                   b'</head>', b'<body>',
                   b'<p><a href="/">&larr; Retour</a></p>',
                   _Conf.diff(params).encode(),
                   b'<p><a href="/">&larr; Retour</a></p>',
                   b'</body>', b'</html>']
        return b'\n'.join(content)


## MAIN ##
if __name__=='__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('file_names', metavar='file', type=str, nargs='+',
                        help='files to analyze')
    parser.add_argument('-B', metavar='b_file', type=str, nargs='+',
                        help='second set of files to compare to the first (default: same as first set)')
    parser.add_argument('--port', type=int, default=PORT,
                        help=f'port to use (default: {PORT})')
    args = parser.parse_args()
    Bfiles = args.B if args.B else args.file_names
    _Conf = PyagiatConfig(args.file_names, Bfiles)
    PORT = args.port
    print(f'Serving on port {PORT}...', end=' ')
    httpd = HTTPServer(('', PORT), PyagiatHTTP)
    httpd_thread = Thread(target=httpd.serve_forever, daemon=True)
    httpd_thread.start()
    print('done')
    print(f'Opening http://localhost:{PORT}/ in a web browser...', end=' ')
    webbrowser.open(f'http://localhost:{PORT}/')
    print('done')
    try:
        httpd_thread.join()
    except KeyboardInterrupt:
        print('Interrupted.', file=sys.stderr)
        sys.exit()
