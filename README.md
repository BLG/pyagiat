# pyagiat

Détecteur de plagiat basé sur une distance de compression normalisée:
```
d(A,B) = (|z(A.B)| - min(|z(A)|,|z(B)|)) / max(|z(A)|,|z(B)|)
```
pour `z()` un algorithme de compression.

`d(A,B)` est proche de 1 si les données sont très différentes et se rapproche d'autant plus de 0 qu'elles sont similaires.

_(Initialement écrit en 2015 pour accompagner la correction de projets universitaires.)_

## Usage

Flag `-h` pour les options. En particuler, `-B` pour spécifier un second ensemble de fichiers à comparer au premier.

### CLI

```
$ ./pyagiat.py files... > out.csv
```

Pour croiser deux ensembles distincts de fichiers :

```
$ ./pyagiat.py a_files... -B b_files... > out.csv
```

### [GUI](screen.png)

```
$ ./pyagiat_gui.py files...
Serving on port 8000... done
Opening http://localhost:8000/ in a web browser... done
```

## Voir aussi

* un [article sur _Interstices_](https://interstices.info/classer-musiques-langues-images-textes-et-genomes/) de J.-P. Delahaye (ou dans [PLS](http://cristal.univ-lille.fr/~jdelahay/pls/115.pdf))
* [Baldr](https://wassner.blogspot.com/2014/05/baldr-loutil-anti-fraude-anti-plagiat.html) [[sourceforge](https://sourceforge.net/projects/baldr/)]
* Bennett _et al._, [Information Distance](https://cs-web.bu.edu/faculty/gacs/papers/info-distance.pdf), 1998
* Li _et al._, [The Similarity Metric](https://arxiv.org/abs/cs/0111054), 2003
* Cilibrasi-Vitanyi, [Clustering by compression](https://arxiv.org/abs/cs/0312044), 2004
* [Moss](https://theory.stanford.edu/%7Eaiken/moss/) and [Winnowing: local algorithms for document fingerprinting](https://theory.stanford.edu/~aiken/publications/papers/sigmod03.pdf), 2003
